#ifndef helpersincluded
	#include <avr/io.h>
	#include <util/delay.h>
	#include <avr/interrupt.h>
	#include <avr/eeprom.h>
	#include <string.h>
	#include <stdlib.h>
	#include <avr/wdt.h>
	#include "lcd/lcd.h"
	#include "can/can.h"
	#define FOSC 7372800// Clock Speed
	#define TICKSPERSECOND FOSC / 256
	#define TICKSPERHALFSECOND TICKSPERSECOND / 2
	#define TICKSPERQUARTERSECOND TICKSPERSECOND / 4
	#define TICKSPEROCTALSECOND TICKSPERSECOND / 8
	#define MINIMUMRPMTICKS TICKSPERSECOND / 333
	#define helpersincluded
#endif


/*
 * TYPEDEF
 */

//struct zum vorhalten der daten
typedef struct {
	uint16_t lastDisplayed;        
    uint16_t iSpeed;
    uint16_t iLastSpeedTime;
	uint16_t iRPM;
	uint16_t iLastRpmTime;
	uint16_t iKM;
    uint16_t iM;
	uint16_t iDKM;
	uint16_t iDM;
	uint16_t iMaxTemp;
	uint16_t iDisplayScenario;
	uint16_t iLambda;
	uint16_t iEGT;
}data;


/*
 * struct zum vorhalten der konfigurationsdaten
 */
typedef struct {
	uint8_t iRadUmfang;
	uint8_t iSigsPerRound;
	uint16_t iWarnTemp;
}config;


/*
 * METHODS
 */

//checks what kind of message was received and puts the info in data
void handleMessage(can_t msg, data *daten){
	if(msg.data[0] == 'l'){
		char value[4];
		int i=2;
		for(;i<msg.length; i++){
			value[i-2] = msg.data[i];
		}
		value[i-2]='\0';
		(*daten).iLambda=atoi(value);
		return;
	}

	if(msg.data[0] == 'e'){
		char value[6];
		int i=2;
		for(;i<msg.length; i++){
			value[i-2] = msg.data[i];
		}
		value[i-2]='\0';
		(*daten).iEGT=atoi(value);
		return;
	}

	if(msg.data[0] == 'r'){
		char value[6];
		int i=2;
		for(;i<msg.length; i++){
			value[i-2] = msg.data[i];
		}
		value[i-2]='\0';
		(*daten).iRPM=atoi(value);
		(*daten).iLastRpmTime=TCNT1;
		return;
	}

	if(msg.data[0] == 'x') {
		//atmega abschiessen
		cli();
		wdt_enable (WDTO_15MS);
		while (1);	
	}
}

//displays the data in a struct of type data
void displayData(data *daten){
	/*
	 * build strings
	 */
	int i;	

	
	//lambda
	char sLambda[8];
	sLambda[0] = 'L';
	sLambda[1] = ':';
	sLambda[2] = ' ';
	char temp[8];
	utoa((*daten).iLambda, temp, 10);
	if((*daten).iLambda<1){
		sLambda[3] = 'N';
		sLambda[4] = '/';
		sLambda[5] = 'A';
		sLambda[6] = '\0';
	}
	else if ((*daten).iLambda<100){
		sLambda[3] = '0';
		sLambda[4] = '.';
		sLambda[5] = temp[0];
		sLambda[6] = temp[1];
		sLambda[7] = '\0';
	}
	else{
		sLambda[3] = temp[0];
		sLambda[4] = '.';
		sLambda[5] = temp[1];
		sLambda[6] = temp[2];
		sLambda[7] = '\0';
	}



	//egt
	char sEGT[8];
	char tempEgt[8];
	utoa((*daten).iEGT, tempEgt, 10);
	sEGT[0] = 'T';
	sEGT[1] = ':';
	sEGT[2] = ' ';

	int j=3;

	if((*daten).iEGT<1000){
		sEGT[3] = ' ';
		j++;
	}
	if((*daten).iEGT<100){
		sEGT[4] = ' ';
		j++;
	}
	if((*daten).iEGT<10){
		sEGT[5] = ' ';
		j++;
	}

	for(i=0;i<4;i++)
	{
		sEGT[i+j] = tempEgt[i];
		if(tempEgt[i]=='\0') 
			break;
		
	}
	sEGT[i+j+1]='\0';


	//rpm
	char trpm[7];
	utoa((*daten).iRPM, trpm, 10);

	char sRPM[9];
	sRPM[0] = 'R';
	sRPM[1] = ':';
	sRPM[2] = ' ';

	if((*daten).iRPM<100){
		sRPM[3] = ' ';
		sRPM[4] = ' ';
		sRPM[5] = ' ';
		sRPM[6] = '0';
		sRPM[7] = '\0';
	}
	else if((*daten).iRPM<1000){
		sRPM[3] = ' ';
		sRPM[4] = '0';
		sRPM[5] = '.';
		sRPM[6] = trpm[0]; 
		sRPM[7] = '\0';
	}
	else{
		if((*daten).iRPM<10000)
		{
			sRPM[3] = ' ';
			sRPM[4] = trpm[0];
			sRPM[5] = '.';
			sRPM[6] = trpm[1]; 
			sRPM[7] = '\0';
		}
		else{
			sRPM[3] = trpm[0];
			sRPM[4] = trpm[1];
			sRPM[5] = '.';
			sRPM[6] = trpm[2]; 
			sRPM[7] = '\0';
		}
	}

	//speed
	char speed[8];
	utoa((*daten).iSpeed, speed, 10);
	char sSpeed[8];
	sSpeed[0] = 'V';
	sSpeed[1] = ':';
	sSpeed[2] = ' ';
	sSpeed[3] = ' ';
	i=0;
	j=4;
	if((*daten).iSpeed<100){
		sSpeed[4] = ' ';
		j++;
	}
	if((*daten).iSpeed<10){
		sSpeed[5] = ' ';
		j++;
	}

	for(; i<3; i++){
		sSpeed[i+j] = speed[i];
		if(speed[i]=='\0')
			break;
	}

	//distance
	char km[6];
	utoa((*daten).iKM, km, 10);
	char  m[2];
	utoa((*daten).iM/100, m, 10);
	char sDistance[7];
	i=0;
	while(km[i]!='\0'){
		sDistance[i] = km[i];
		i++;
	}
	sDistance[i]='.';
	sDistance[i+1]=m[0];
	sDistance[i+2]='\0';

	//tageskilometer
	char dkm[6];
	utoa((*daten).iDKM, dkm, 10);
	char  dm[2];
	utoa((*daten).iDM/100, dm, 10);
	char sDayDistance[8];
	sDayDistance[0] = 'D';
	sDayDistance[1] = ':';
	i=0;
	j=2;
	if((*daten).iDKM<100){
		sDayDistance[2] = ' ';
		j++;
	}
	if((*daten).iDKM<10){
		sDayDistance[3] = ' ';
		j++;
	}

	while(dkm[i]!='\0' && i<4){
		sDayDistance[i+j] = dkm[i];
		i++;
	}
	sDayDistance[i+j]='.';
	sDayDistance[i+j+1]=dm[0];
	sDayDistance[i+j+2]='\0';


	/*
	 * build output strings
	 */
	char ol[10];
	char or[8];
	char ul[10];
	char ur[8];

	switch((*daten).iDisplayScenario){
		case 0:
			strcpy(ol,sSpeed);
			strcpy(or,sRPM);
			strcpy(ul,sDistance);
			strcpy(ur,sDayDistance);
		break;

		case 1:
			strcpy(ol,sSpeed);
			strcpy(or,sRPM);
			strcpy(ul,sLambda);
			strcpy(ur,sEGT);
		break;

		case 2:
			strcpy(ol,sRPM);
			strcpy(or,sEGT);
			strcpy(ul,sSpeed);
			strcpy(ur,sLambda);
		break;
		
		default:
			strcpy(ol,sSpeed);
			strcpy(or,sRPM);
			strcpy(ul,sDistance);
			strcpy(ur,sDayDistance);
			(*daten).iDisplayScenario = 0;
		break;
	}


	/*
	 * output data
	 */
	lcd_clrscr();
	lcd_gotoxy(0,0);
	lcd_puts(ol);
	lcd_gotoxy(9,0);
	lcd_puts(or);
	lcd_gotoxy(0,1);
	lcd_puts(ul);
	lcd_gotoxy(9,1);
	lcd_puts(ur);
}

//method to enable/disable the displays led
char lcd_bel(char opt){
	if(opt==0){ 	
		PORTA &= ~(1<<PA0); //beleuchtung aus
		return 0;
	}
	else{
		PORTA |= (1<<PA0); //beleuchtung an
		return 1;
	}
}

//method to debounce a button
uint8_t fdebounce(volatile uint8_t *port, uint8_t pin){
    if ( ! (*port & (1 << pin)) ){
	    _delay_ms(50); 
        _delay_ms(50); 
        if ( *port & (1 << pin) ){
            _delay_ms(50);
            _delay_ms(50); 
            return 1;
        }
    }
    return 0;
}

//menu
void menu(uint16_t *eeDKM,uint16_t *eeMaxTemp,uint16_t *eeWarnTemp,uint16_t *eeIgnitions,uint16_t *eeRadUmfang, data *daten, config *configuration)
{
	lcd_bel(1);
	lcd_clrscr();

	char menu=0;
	uint8_t state=1;
	uint16_t ignitions;
	uint16_t radumfang;
	uint16_t warntemp;
	lcd_gotoxy(0, 0);
	lcd_puts("->,Enter");
	while(menu<1)
	{
			switch(state)
			{
				case 1:
					lcd_gotoxy(0,1);
					lcd_puts("Tag-Km=0");
					while(state<2)
					{
						if (fdebounce(&PINA, PA1)==1)
						{
							eeprom_write_word(eeDKM, 0);
							(*daten).iDKM=0;
							(*daten).iDM=0;
							lcd_gotoxy(0,1);
							lcd_puts("  DONE  ");
							_delay_ms(500);
							state=2;
						}
						if (fdebounce(&PINA, PA2)==1) state=2;
					}
				break;

				case 2:
					lcd_gotoxy(0,1);
					lcd_puts("MxTemp=0");
					while(state<3)
					{
						if (fdebounce(&PINA, PA1)==1) 
						{
							eeprom_write_word(eeMaxTemp, 0);
							lcd_gotoxy(0,1);
							lcd_puts("  DONE  ");
							_delay_ms(500);
							state=3;
						}

						if (fdebounce(&PINA, PA2)==1) state=3;
					}
				break;

				case 3:
					ignitions=(*configuration).iSigsPerRound;
					while(state<4)
					{
						lcd_gotoxy(0,1);
						char outs1[3];
						itoa(ignitions,outs1,10);
						char outs2[8];
						strcpy(outs2,"Sig/U: ");
						strcat(outs2,outs1);
						lcd_puts(outs2);
						if (fdebounce(&PINA, PA1)==1) 
						{
							if(ignitions<6) ignitions++;
							else ignitions=1;
						}

						if (fdebounce(&PINA, PA2)==1)
						{
							state=4;
							if((*configuration).iSigsPerRound!=ignitions){
								eeprom_write_word(eeIgnitions,ignitions);
								(*configuration).iSigsPerRound=ignitions;
							}							
						}
					}
				break;

				case 4:
					radumfang=(*configuration).iRadUmfang;
					while(state<5)
					{					
						lcd_gotoxy(0,1);
						char out1[3];
						itoa(radumfang,out1,10);
						char out2[8];
						strcpy(out2,"Umfa:");
						if(radumfang<100) strcat(out2," ");
						if(radumfang<10) strcat(out2," ");
						strcat(out2,out1);
						lcd_puts(out2);
						if (fdebounce(&PINA, PA1)==1) 
						{
							if(radumfang<200) radumfang++;
							else radumfang=1;
						}
									
						if (fdebounce(&PINA, PA2)==1) 
						{
							state=5;
							if((*configuration).iRadUmfang!=radumfang){
								eeprom_write_word(eeRadUmfang,radumfang);
								(*configuration).iRadUmfang = radumfang;
							}
						}
					}
				break;

				case 5:
					warntemp = (*configuration).iWarnTemp;
					while(state<6)
					{
						lcd_gotoxy(0,1);
						char out1[4];
						itoa(warntemp,out1,10);
						char out2[9];
						strcpy(out2,"WTp:");
						if(warntemp<1000) strcat(out2," ");
						if(warntemp<100) strcat(out2, " ");
						if(warntemp<10) strcat(out2, " ");
						
						
						strcat(out2,out1);
						
						lcd_puts(out2);
						
						if (fdebounce(&PINA, PA1)==1) 
						{
							if(warntemp<1200) warntemp+=5;
							else warntemp=1;
						}
									
						if (fdebounce(&PINA, PA2)==1) 
						{
							if((*configuration).iWarnTemp != warntemp){
								eeprom_write_word(eeWarnTemp, warntemp);
								(*configuration).iWarnTemp = warntemp;
							}
							state=6;
						}
					}
				break;

				case 6:
						lcd_gotoxy(0, 1);
						lcd_puts("->,Close");
						while(state<7)
						{
							if (fdebounce(&PINA, PA2)==1) state=8;
							if (fdebounce(&PINA, PA1)==1) 
								return;
							
						}
				break;

				default:
					state=1;
				break;
			}
	}
}
