#ifndef helpersincluded
	#include "helpers.c"
#endif

/*
 * VAR
 */

//eeprom var
uint16_t EEMEM eeKM;
uint16_t EEMEM eeM;
uint16_t EEMEM eeDKM;
uint16_t EEMEM eeDM;
uint16_t EEMEM eeRadUmfang;
uint16_t EEMEM eeIgnitions;
uint16_t EEMEM eeWarnTemp;
uint16_t EEMEM eeMaxTemp;

//struct zum vorhalten der daten
data daten;

//struct zum vorhalten der konfigurationsdaten
config configuration;

//can message
can_t can_received_msg;

//time of last tacho impulse 
volatile uint16_t tacho_old=0;
//timediff for calc
volatile uint16_t tacho_diff=0;
//signals for distance7372800
volatile uint8_t tacho_signals=0;
//var for storing ran centimeters
uint16_t cm_ran = 0;

//the old timestamp of rpm
volatile uint16_t dzm_old=0;
//the timediff
volatile uint16_t dzm_diff=0;

/*
 * ISR
 */

//tacho interrupt-routine
ISR(INT0_vect){
	tacho_signals++;

	if(tacho_old == 0)
		tacho_diff = 0;
	else
		tacho_diff = TCNT1 - tacho_old;

	tacho_old = TCNT1;
	daten.iLastSpeedTime = TCNT1;
}

//rpm interrupt routine
ISR(INT1_vect){
	//software-entprellung
	if(TCNT1-dzm_old <= MINIMUMRPMTICKS)
		return;

	if(dzm_old == 0)
		dzm_diff = 0;
	else
		dzm_diff = TCNT1-dzm_old;

	dzm_old = TCNT1;
	daten.iLastRpmTime=TCNT1;
}

//interrupt-routine for power loose notification
ISR ( ANA_COMP_vect ){
	//disbale interrupts
	cli();

	//shut off display light
	lcd_bel(0);

	//save data
	eeprom_write_word(&eeKM, daten.iKM);
  	eeprom_write_word(&eeM, daten.iM);
  	eeprom_write_word(&eeDKM, daten.iDKM);
	eeprom_write_word(&eeDM, daten.iDM);
	eeprom_write_word(&eeMaxTemp, daten.iMaxTemp);

	//give an error message (on real power loose this will not be visible)
  	lcd_clrscr();
  	_delay_ms(1);
  	lcd_puts("ERROR 0x00"); 
	lcd_gotoxy(0,1);
	lcd_puts("CONTACT SUPPLIER"); 
  	lcd_bel(1);

	//wait for the user to give ok in case of an error
	while(1)
		if (fdebounce(&PINA, PA1)==1 || fdebounce(&PINA, PA2)==1) 
			break;

	//enable interrupts, continue
	sei();
}


/*
 * INIT
 */

//register - settings
void init_registers(){
	//systemtime-timer (clock/256)
	TCCR1B = (1<<CS12);
	
	//ana compare interrupt (power loose), clear aci for events until now
	//acis0=0 falling edge, acis0=1 rising edge acis0+1=0 toggle
	ACSR = (1 << ACIE) | (1 << ACI) | (1 << ACIS1); //(1 << ACIS0)

	//tacho, falling edge
	MCUCR |= (1<<ISC01)| (1<<ISC00);
	GIMSK |= (1<<INT0);

	//rpm, rising edge
	MCUCR |= (1<<ISC11) | (1<<ISC10);
	GIMSK |= (1<<INT1);

	//rpm d2, v d3
	//display: a0,a4-7,d4-6 (light:PA0)
	//all pull-ups high except display
	PORTA = 0b00001110;
	PORTB = 0b11111111;
	PORTC = 0b11111111;
	PORTD = 0b10000011;

	//all pins as input except display
	DDRA = 0b11110001;
	DDRB = 0b00000000;
	DDRC = 0b00000000;
	DDRD = 0b01110000;
}

//prevent interrupt triggers when unwanted
void init_clear_interrupts(){
	ACSR |= (1 << ACI);
}

//welcome message
void init_messages(){
	lcd_clrscr();
	lcd_puts("VesComIII SWv0.1");
	_delay_ms(500);
	lcd_gotoxy(0,1);
	lcd_puts("   by VesTech");
	_delay_ms(2000);
	lcd_clrscr();	
}

//can - init
void init_can(){
	//can init
	if(!can_init(BITRATE_125_KBPS)){
		lcd_puts("CAN_init_FAILED!...");
		while(1);
	}

	can_filter_t filter = {
    	.id = 0x100,
    	.mask = 0,
    	.flags = {
        	.rtr = 0,
        	.extended = 0
    	}
	};

	can_set_filter(0, &filter);

	// Create can Messages
	can_t msg;	

	msg.id = 0x1FFFFFFE;
	msg.flags.rtr = 0;
	msg.flags.extended = 1;
	msg.length = 8;
	
	char* m1 = "VesTech.";
	char* m2 = "VCIII...";
	char* m3 = "Init OK.";

	for(int i=0;i<8;i++){
		msg.data[i]=m1[i];
	}

	can_send_message(&msg);
	

	for(int i=0;i<8;i++){
		msg.data[i]=m2[i];
	}

	_delay_ms(100);
	can_send_message(&msg);	

	for(int i=0;i<8;i++){
		msg.data[i]=m3[i];
	}

	_delay_ms(100);
	can_send_message(&msg);
}

//init - load eeprom settings
void init_read_eeprom(){
	if(eeprom_read_word(&eeRadUmfang)==0xFFFF){configuration.iRadUmfang=127;eeprom_write_word(&eeRadUmfang,127);}
	else configuration.iRadUmfang=eeprom_read_word(&eeRadUmfang);

	if(eeprom_read_word(&eeIgnitions)==0xFFFF){configuration.iSigsPerRound=2;eeprom_write_word(&eeIgnitions,2);}
	else configuration.iSigsPerRound=eeprom_read_word(&eeIgnitions);

	if(eeprom_read_word(&eeKM)==0xFFFF){daten.iKM=0;eeprom_write_word(&eeKM,0);}
	else daten.iKM=eeprom_read_word(&eeKM);

	if(eeprom_read_word(&eeM)==0xFFFF){daten.iM=0;eeprom_write_word(&eeM,0);}
	else daten.iM=eeprom_read_word(&eeM);

	if(eeprom_read_word(&eeDKM)==0xFFFF){daten.iDKM=0;eeprom_write_word(&eeDKM,0);}
	else daten.iDKM=eeprom_read_word(&eeDKM);

	if(eeprom_read_word(&eeDM)==0xFFFF){daten.iDM=0;eeprom_write_word(&eeDM,0);}
	else daten.iDM=eeprom_read_word(&eeDM);

	if(eeprom_read_word(&eeWarnTemp)==0xFFFF){configuration.iWarnTemp=750;eeprom_write_word(&eeWarnTemp,750);}
	if(eeprom_read_word(&eeMaxTemp)==0xFFFF){daten.iMaxTemp=0;eeprom_write_word(&eeMaxTemp,0);}
}

//init - method
void init(){

	//disable interrupts
	cli();

	//genulle
	daten.lastDisplayed=0;
	daten.iSpeed = 0;
	daten.iDisplayScenario = 0;
	daten.iRPM = 0;

	//atmega initialisieren
	init_registers();
	
	//read data from eeprom
	init_read_eeprom();

	//lcd initialisieren
	lcd_bel(1);
	_delay_ms(100);
	lcd_init(LCD_DISP_ON);
	
	//can initialisieren
	init_can();

	//clean init failures
	DDRD  &= ~(1<<DDD3);
	//PORTD |= (1<<PD3);
	DDRD  &= ~(1<<DDD2);
	PORTD &= ~ (1<<PD2);

	//messages
	init_messages();

	//enable interrupts
	init_clear_interrupts();
	sei();
}

/*
 * Main_Program
 */
int main(void)
{
	init();

	while(1){
		/*
		 * Buttons
		 */
		int show_menu = 1;
		if (fdebounce(&PINA, PA1)==1) {
			for(int i=0;i<250;i++){
				_delay_ms(1);
				if(fdebounce(&PINA, PA1)==1){
					for(int i=0;i<500;i++){
						_delay_ms(1);
						if(fdebounce(&PINA, PA2)==1){
							for(int i=0;i<750;i++){
								_delay_ms(1);
								if(fdebounce(&PINA, PA1)==1){
									lcd_clrscr();
									lcd_puts(" i love bambi!!");
									_delay_ms(1000);
									lcd_clrscr();
									show_menu=0;
								}
							}
						}
					}
				}
			}

			if(show_menu==1){
				lcd_clrscr();
				menu(&eeDKM,&eeMaxTemp,&eeWarnTemp,&eeIgnitions,&eeRadUmfang, &daten, &configuration);
			}
			lcd_clrscr();
   		}
		if (fdebounce(&PINA, PA2)==1) {
			daten.iDisplayScenario++;
   		}

		/*
		 * CHECK FOR CAN MSG
		 */
		can_t msg2;
		// Try to read the message
		if (can_get_message(&msg2)){
			handleMessage(msg2, &daten);
		}

		/*
		 * DATEN
		 */
		if(daten.lastDisplayed == 0 || TCNT1-daten.lastDisplayed>(TICKSPEROCTALSECOND)){
			/*
			 * RPM
			 */
			//rpm=0 if no signal for more than 1/2 second
			if((TCNT1 - dzm_old) > TICKSPERHALFSECOND){
				dzm_diff=0;
				dzm_old=0;
			}
			//rpm build
			if(dzm_diff>0 && dzm_diff>MINIMUMRPMTICKS/configuration.iSigsPerRound)
				daten.iRPM = TICKSPERSECOND/dzm_diff * 60 / configuration.iSigsPerRound;

			//rpm clear
			if((TCNT1 - daten.iLastRpmTime) > TICKSPERHALFSECOND)
				daten.iRPM = 0;	
			/*
			 * TACHO / DISTANCE
			 */
			//tacho=0 if no signal for more than a second
			if(TCNT1-tacho_old > TICKSPERSECOND){
				tacho_diff=0;
				tacho_old=0;
			}
			//tacho auswerten
			if(tacho_diff>0)
				daten.iSpeed = TICKSPERSECOND * configuration.iRadUmfang * 36/ tacho_diff / 1000;

			//clear tacho
			if((TCNT1 - daten.iLastSpeedTime) > TICKSPERSECOND)
				daten.iSpeed = 0;

			//ticks auswerten
			cm_ran += configuration.iRadUmfang * tacho_signals;
			tacho_signals = 0;
			
			uint16_t meters_ran = 0;

			while(cm_ran > 99){
				cm_ran-=100;
				meters_ran += 1;
			}

			daten.iM += meters_ran;
			daten.iDM += meters_ran;
			
			if(daten.iM > 999){
				daten.iM -= 1000;
				daten.iKM += 1;
			}

			if(daten.iDM > 999){
				daten.iDM -= 1000;
				daten.iDKM += 1;
			}

			if(daten.iKM > 99999)
				daten.iKM = 0;

			if(daten.iDKM > 999)
				daten.iDKM = 0;

			displayData(&daten);
			daten.lastDisplayed = TCNT1;
		}
		
	}
}
