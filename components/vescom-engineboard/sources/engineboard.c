#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <string.h>
#include "can/can.h"
#include <avr/wdt.h>
#include "lambda.c"
#include <stdlib.h>



#define FOSC 7730941// Clock Speed
#define ticksPerSecond FOSC / 256
#define ticksPerHalfSecond ticksPerSecond / 2


char signalsPerRound = 2;
volatile uint16_t dzmold=0;
volatile uint16_t dzm_delta=0;
uint16_t dzmnew=0;
uint16_t lastLog=0;
uint16_t lastTimeDiff=0;
uint16_t lastEGTmeasure=0;
char rpm[8];
char lambda[8];
char egt[8];



ISR(INT0_vect)
{
	if(dzmold != 0)
	{
		dzm_delta = TCNT1-dzmold;
	}
	dzmold = TCNT1;
}


int main(void){


	//start timer for systemtime CLK_DIV=256(30199 ticks/s)
   	TCCR1B |= (1<<CS12);

	//fallende flanke l�st interrupt aus
	MCUCR |= (1<<ISC01);
   	//enable interrupt on int0
	GIMSK |= (1<<INT0);

	_delay_ms(1000);

	//can init
	if(!(can_init(BITRATE_125_KBPS)))
	{
		_delay_ms(500);
		if(!(can_init(BITRATE_125_KBPS))) 
			cli();wdt_enable (WDTO_15MS);while (1); //atmega abschiessen
	}

	can_filter_t filter = {
    	.id = 0x100,
    	.mask = 0,
    	.flags = {
        	.rtr = 0,
        	.extended = 0
    	}
	};

	can_set_filter(0, &filter);

	sei();

	// Create Messages
	can_t msg;
	
	msg.id = 0x1FFFFFFE;
	msg.flags.rtr = 0;
	msg.flags.extended = 1;
	msg.length = 8;
	
	char* m1 = "VesTech.";
	char* m2 = "ENG2CAN.";
	char* m3 = "Init OK.";

	for(int i=0;i<8;i++){
		msg.data[i]=m1[i];
	}
	
	_delay_ms(1000);
	can_send_message(&msg);
	

	for(int i=0;i<8;i++){
		msg.data[i]=m2[i];
	}

	_delay_ms(200);
	can_send_message(&msg);	

	for(int i=0;i<8;i++){
		msg.data[i]=m3[i];
	}

	_delay_ms(200);
	can_send_message(&msg);



	for(int i=0;i<8;i++){
		msg.data[i]=m2[i];
	}

	lambda_init();
	egt_init();
	lastLog = TCNT1;
	lastEGTmeasure=TCNT1;

	while(1){
		
		//grab data from interrupt routine
		cli();
		dzmnew=dzm_delta;
		sei();
		
		//last rpm signal too old?
		if((TCNT1 - dzmold) > ticksPerHalfSecond)
		{
			dzmnew=0;
		}

		//rpm string build
		if(dzmnew>0)
		{
			dzmnew = ticksPerSecond/dzmnew * 60 / signalsPerRound;
			dzmold = TCNT1;
		}		

		char sDZM[8];
		utoa(dzmnew,sDZM,10);
		rpm[0]='r';
		rpm[1]=',';
		uint8_t i = 0;
		while (sDZM[i]!='\0' && i<6)
		{
			rpm[i+2] = sDZM[i];
			i++;
		}
		rpm[i+2] = '\0';


		if((TCNT1-lastEGTmeasure) > ticksPerHalfSecond)
		{
			//egt string build
			char tegt[8];
			uint16_t tempegt = get_egt();
			utoa(tempegt,tegt,10);
			egt[0]='e';
			egt[1]=',';
			i = 0;
			while (tegt[i]!='\0' && i<6)
			{
				egt[i+2] = tegt[i];
				i++;
			}
			egt[i+2] = '\0';
			lastEGTmeasure = TCNT1;
		}
						
	
		//lambda string build
		lambda[0]='l';
		lambda[1]=',';
		if(get_lambda_status() == lambda_heat) 
		{
			lambda[2]='0';
			lambda[3]='\0';
		}
		else{
			uint16_t ilambda = get_lambda();
			char slambda[8];
			utoa(ilambda,slambda,10);
			uint8_t i = 2;
			if(ilambda<100)
			{
				lambda[i] = '0';
				lambda[i+1] = '.';
				i+=2;
			}
			lambda[i] = slambda[0];
			i++;
			if(ilambda>=100)
			{
				lambda[i] = '.';
				i++;
			}
						
			uint8_t j=1;
			while(slambda[j]!='\0' && j<6)
			{
				lambda[i] = slambda[j];
				i++;
				j++;				
			}
		}	
		

		//time for a new log?
		if((TCNT1 - lastLog) > ticksPerHalfSecond)
		{
			uint16_t timeDiff = TCNT1 - lastLog;
			lastLog=TCNT1;

			if(lastTimeDiff == timeDiff) timeDiff = timeDiff-1;
			lastTimeDiff = timeDiff;

			msg.id = timeDiff;

			//initial message for a new log dataset
			msg.data[0] = 'n';
			msg.data[1] = 'e';
			msg.data[2] = 'w';
			msg.data[3] = ',';
			msg.data[4] = 'l';
			msg.data[5] = 'o';
			msg.data[6] = 'g';
			msg.length = 7;
			can_send_message(&msg);
			_delay_ms(10);

			//egt message
			uint8_t i=0;
			while(egt[i]!='\0' && i<8)
			{
				msg.data[i] = egt[i];
				i++;
			}
			msg.length = i;
			can_send_message(&msg);
			_delay_ms(10);

			//lambda message
			i=0;
			while(lambda[i]!='\0' && i<8)
			{
				msg.data[i] = lambda[i];
				i++;
			}
			msg.length = i;
			can_send_message(&msg);
			_delay_ms(10);

			//rpm message
			i=0;
			while(rpm[i]!='\0' && i<8)
			{
				msg.data[i] = rpm[i];
				i++;
			}
			msg.length = i;
			can_send_message(&msg);
		}

		_delay_ms(200);
	}

}
