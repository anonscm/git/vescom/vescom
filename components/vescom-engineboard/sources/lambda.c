#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <string.h>

//lambda defines
#define lambda_linear_port PORTA
#define lambda_linear PINA7
#define lambda_status_pin PINC
#define lambda_status0 PINC6
#define lambda_status1 PINC7
#define lambda_heat 1
#define lambda_ready 0

//egt defines
#define EGT_PORT PORTA
#define EGT_PIN PINA
#define EGT_DATA PINA6
#define EGT_CS PA5
#define EGT_CLK PA4


void egt_init()
{
	DDRA |= (1 << DDA5) | (1 << DDA4);
	PORTA |= (1<<PA5); //cs high for measure
}

uint16_t get_egt()
{
	uint16_t temp;

	_delay_ms(1); //time for ic to chill
	EGT_PORT |= (1<<EGT_CS); //cs high for measure
	EGT_PORT &= ~(1<<EGT_CLK); //clk low but dontcare
	_delay_ms(1);
	EGT_PORT &= ~(1<<EGT_CS); //cs low for data
	_delay_ms(1);
	EGT_PORT |= (1<<EGT_CLK); //clk high 
	_delay_ms(1);
	EGT_PORT &= ~(1<<EGT_CLK); //clk low
	_delay_ms(1);
	if ( EGT_PIN & (1<<EGT_DATA) ) temp=512; else temp=0;
	_delay_ms(1);
	EGT_PORT |= (1<<EGT_CLK); //clk high 
	_delay_ms(1);
	EGT_PORT &= ~(1<<EGT_CLK); //clk low
	_delay_ms(1);
	if ( EGT_PIN & (1<<EGT_DATA) ) temp+=256;
	_delay_ms(1);
	EGT_PORT |= (1<<EGT_CLK); //clk high 
	_delay_ms(1);
	EGT_PORT &= ~(1<<EGT_CLK); //clk low
	_delay_ms(1);
	if ( EGT_PIN & (1<<EGT_DATA) ) temp+=128;
	_delay_ms(1);
	EGT_PORT |= (1<<EGT_CLK); //clk high 
	_delay_ms(1);
	EGT_PORT &= ~(1<<EGT_CLK); //clk low
	_delay_ms(1);
	if ( EGT_PIN & (1<<EGT_DATA) ) temp+=64;
	_delay_ms(1);
	EGT_PORT |= (1<<EGT_CLK); //clk high 
	_delay_ms(1);
	EGT_PORT &= ~(1<<EGT_CLK); //clk low
	_delay_ms(1);
	if ( EGT_PIN & (1<<EGT_DATA) ) temp+=32;
	_delay_ms(1);
	EGT_PORT |= (1<<EGT_CLK); //clk high 
	_delay_ms(1);
	EGT_PORT &= ~(1<<EGT_CLK); //clk low
	_delay_ms(1);
	if ( EGT_PIN & (1<<EGT_DATA) ) temp+=16;
	_delay_ms(1);
	EGT_PORT |= (1<<EGT_CLK); //clk high 
	_delay_ms(1);
	EGT_PORT &= ~(1<<EGT_CLK); //clk low
	_delay_ms(1);
	if ( EGT_PIN & (1<<EGT_DATA) ) temp+=8;
	_delay_ms(1);
	EGT_PORT |= (1<<EGT_CLK); //clk high 
	_delay_ms(1);
	EGT_PORT &= ~(1<<EGT_CLK); //clk low
	_delay_ms(1);
	if ( EGT_PIN & (1<<EGT_DATA) ) temp+=4;
	_delay_ms(1);
	EGT_PORT |= (1<<EGT_CLK); //clk high 
	_delay_ms(1);
	PORTA &= ~(1<<EGT_CLK); //clk low
	_delay_ms(1);
	if ( EGT_PIN & (1<<EGT_DATA) ) temp+=2;
	_delay_ms(1);
	PORTA |= (1<<EGT_CLK); //clk high 
	_delay_ms(1);
	PORTA &= ~(1<<EGT_CLK); //clk low
	_delay_ms(1);
	if ( EGT_PIN & (1<<EGT_DATA) ) temp+=1;

	EGT_PORT &= ~(1<<EGT_CLK); //clk low
	_delay_ms(1);
	EGT_PORT |= (1<<EGT_CS); //cs high for measure
	EGT_PORT &= ~(1<<EGT_CLK); //clk low but dontcare	

	return temp;

}

void lambda_init()
{
	ADMUX = (1<<MUX2) |(1<<MUX1) | (1<<MUX0) | (1<<ADLAR); //einstellen auf ADC3 f�r lambda-messung, ergebnis linksb.
	ADCSRA= (1<<ADEN) | (1<<ADSC) | (1<<ADATE) | (1<<ADPS2) | (1<<ADPS1); //adc enable und start, free-run, clk/64
}

char get_lambda_status()
{
	if ( lambda_status_pin & (1<<lambda_status0) )
	{
		return lambda_heat;
	}
	else
	{
		return lambda_ready;
	}
}

uint16_t get_lambda()
{
	uint16_t lambda = ADCH;
	return 68 + lambda*27 / 100 ;
}
