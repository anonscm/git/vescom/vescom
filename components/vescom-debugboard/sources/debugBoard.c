#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define ticksPerRound 3
#define TPS F_CPU/64

volatile uint16_t rpmtime;
volatile uint16_t vtime;
volatile uint16_t lastrpm;
volatile uint16_t lastv;
volatile uint16_t lasttick;
volatile uint8_t arrayIndex;
volatile uint16_t rpmAndSpeedValues[9][2];

void toggleRPM(){
	if (PINA & (1<<PINA1))  PORTA &= ~(1<<PA1);
	else PORTA |= (1<<PA1);
	lastrpm=TCNT1;
}

void toggleV(){
	if (PINA & (1<<PINA0))  PORTA &= ~(1<<PA0);
	else PORTA |= (1<<PA0);
	lastv=TCNT1;
}

void nextTick(){
	arrayIndex++;
	if(arrayIndex>8)arrayIndex=0;
	rpmtime = rpmAndSpeedValues[arrayIndex][0];
	vtime= rpmAndSpeedValues[arrayIndex][1];
	lasttick=TCNT1;
}

int main(void){
	rpmAndSpeedValues[0][0] = 196;	//ca. 797
	rpmAndSpeedValues[0][1] = 6250;	//ca. 6

	rpmAndSpeedValues[1][0] = 10000;//shall be 0
	rpmAndSpeedValues[1][1] = 10000;//shall be 0

	rpmAndSpeedValues[2][0] = 78;	//ca. 2003
	rpmAndSpeedValues[2][1] = 1956;	//ca. 19

	rpmAndSpeedValues[3][0] = 10000;//shall be 0
	rpmAndSpeedValues[3][1] = 10000;//shall be 0

	rpmAndSpeedValues[4][0] = 40;	//ca. 3906
	rpmAndSpeedValues[4][1] = 390;	//ca. 95

	rpmAndSpeedValues[5][0] = 10000;//shall be 0
	rpmAndSpeedValues[5][1] = 10000;//shall be 0

	rpmAndSpeedValues[6][0] = 8; 	//ca.19531
	rpmAndSpeedValues[6][1] = 196;	//ca. 190

	rpmAndSpeedValues[7][0] = 10000;//shall be 0
	rpmAndSpeedValues[7][1] = 10000;//shall be 0

	rpmAndSpeedValues[8][0] = 10000;//shall be 0
	rpmAndSpeedValues[8][1] = 10000;//shall be 0

	rpmtime = rpmAndSpeedValues[8][0];
	vtime = rpmAndSpeedValues[8][1];

	//systemtime-timer (clock/1024)
	TCCR1B = (1<<CS10) | (1<<CS11);

	arrayIndex=7;

	rpmtime = rpmAndSpeedValues[6][0];
	vtime = rpmAndSpeedValues[6][1];


	vtime=10000;
	rpmtime=5000;

	while(1){
		if(rpmtime!=10000)
				if((TCNT1-lastrpm) >= rpmtime)
					toggleRPM();
	}


	while(1){
		if((TCNT1-lasttick) >= (TPS*2)){
			nextTick();
		}

		if(rpmtime!=10000)
			if((TCNT1-lastrpm) >= rpmtime)
				toggleRPM();

		if(vtime!=10000)
			if((TCNT1-lastv) >= vtime)
				toggleV();
	}
}
